
# coding: utf-8

# # Solution to Excercise 1

# In[ ]:

import datetime

import matplotlib.pyplot as plt
import numpy as np
get_ipython().magic('matplotlib inline')


# ## Task 1: Read the NO2 data from one of the `*.VisNO2A` files.

# In Python, numbers, dates, and other data types can be formatted with the `format` function.  Placeholders can be put into strings using the `{identifier:formatcode}` syntax.  For more information, see https://docs.python.org/3.5/library/string.html#format-string-syntax or https://pyformat.info/.

# In[ ]:

# Set variables for the filename and date and azimuth values
FN_NO2 = '../data/no2-athens/{date:%y%m%d}{azimuth:}S.VisNO2A'
DATE = datetime.date(2013, 6, 24)
AZIM = 'V'


# Here, we directly created a `datetime.date` object for the storing the date.  Alternatively, one could create the date object from a string.  A list of the available format strings is available at https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior.  There are two different data types for dates (`datetime.date`) and timestamps (`datetime.datetime`).  The `date` object represents a date (as in a calendar), and the `datetime` object represents a unique point in time and can be equipped with timezone information.

# In[ ]:

DATE2str = '2013-06-24'
DATE2 = datetime.datetime.strptime(DATE2str, '%Y-%m-%d').date()

# or, different format:

DATE3str = '24.06.2013'
DATE3 = datetime.datetime.strptime(DATE3str, '%d.%m.%Y').date()

# test if the three dates are actually the same
assert DATE == DATE2 == DATE3


# Finally, we can load the data

# In[ ]:

filename = FN_NO2.format(date=DATE, azimuth=AZIM)
d_no2 = np.loadtxt(filename, comments='*')


# ## Task 2: Read and plot the wind data
# 
# Read the wind data from the file `Meteo_HOURLY_DATA_OCT_2012_DEC_2013.csv` and create a plot of wind speed vs. time for the same day which you chose in the previous step.

# In[ ]:

# read the meteorological data
FN_MET = '../data/no2-athens/Meteo_HOURLY_DATA_OCT_2012_DEC_2013.csv'
d_met = np.genfromtxt(FN_MET, skip_header=1, delimiter=',')


# `datetime.date` objects can also be formatted with their `strftime()` function:

# In[ ]:

# determine julian day for the chosen date
jday = int(DATE.strftime('%j'))

# this is be equivalent to
jday2 = int('{date:%j}'.format(date=DATE))

# an alternative way for getting the Julian day, maybe more correct:
jday3 = DATE.timetuple().tm_yday

assert jday == jday2 == jday3


# In[ ]:

# plot the wind data
d_met_2013 = d_met[d_met[:, 0] == 2013]
d_met_day = d_met_2013[d_met_2013[:, 1] == jday]
hours = d_met_day[:, 2] / 100
windspeed = d_met_day[:, 5]
plt.plot(hours, windspeed)
plt.title('Wind speed on {:%d %b %Y}'.format(DATE))
plt.xlabel('Time (hours)')
plt.ylabel('Wind speed (m/s)')
plt.xlim((0, 24))


# ## Task 3: Plot NO2 values
# 
# Create a figure with two plots: one plot of all NO2 measurements for the whole day (NO2 vs. time) and a histogram of all NO2 measurements.

# In[ ]:

# define some index variables, to make the code easier to understand
ix_time = 2
ix_no2 = 13
ix_elev = 6


# In[ ]:

fig, axs = plt.subplots(1, 2, figsize=(12, 4))  # figsize is x,y in inches
axs[0].plot(d_no2[:, ix_time], d_no2[:, ix_no2])
axs[0].set_xlabel('Time (hours)')
axs[0].set_ylabel('NO2 slant column (molec./cm²)')
axs[1].hist(d_no2[:, ix_no2])
axs[1].set_xlabel('NO2 slant column (molec./cm²)')
axs[1].set_ylabel('Occurences')
fig.tight_layout()


# ## Task 4: Create a figure with one plot for each elevation angle (NO2 vs. time).
# 

# First we check which elevation angles are contained in the file:

# In[ ]:

elevations = np.unique(d_no2[:, ix_elev])


# In[ ]:

fig, axs = plt.subplots(2, 4, figsize=(12, 7))
axs = axs.ravel()
for i in range(len(elevations)):
    d_tmp = d_no2[d_no2[:, ix_elev] == elevations[i]]
    axs[i].plot(d_tmp[:, ix_time], d_tmp[:, ix_no2])
    axs[i].set_title('{}°'.format(elevations[i]))
    axs[i].set_xlabel('Time (hours)')
    axs[i].set_xlim((0, 24))
fig.tight_layout()


# ## Task 5
# 
# Determine main statistics (min, max, mean, standard deviation) for the NO2 slant columns for each elevation angle.
# 

# In[ ]:

for elev in elevations:
    print('Statistics for elevation angle {}°'.format(elev))
    print('Minimum NO2 {}'.format(np.min(d_no2[d_no2[:, ix_elev] == elev, ix_no2])))
    print('Maximum NO2 {}'.format(np.max(d_no2[d_no2[:, ix_elev] == elev, ix_no2])))
    print('Mean NO2 {}'.format(np.mean(d_no2[d_no2[:, ix_elev] == elev, ix_no2])))
    print('Std NO2 {}'.format(np.std(d_no2[d_no2[:, ix_elev] == elev, ix_no2])))
    print()

