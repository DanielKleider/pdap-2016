#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:t reveal_control:t
#+OPTIONS: reveal_mathjax:t reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: reveal_width:1024 reveal_height:768
#+OPTIONS: reveal_slide_number:c/t
#+OPTIONS: toc:1 todo:nil timestamp:nil
#+REVEAL_MARGIN: 0.2
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_TRANS: none
#+REVEAL_THEME: solarized
#+REVEAL_HLEVEL: 999
#+REVEAL_PLUGINS: (highlight)
#+REVEAL_ROOT: https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.3.0/

#+TITLE: Practical Data Analysis with Python (PDAP): Session 03
#+AUTHOR: Dr. Andreas Hilboll
#+EMAIL: hilboll@uni-bremen.de
#+DATE: 10 November 2016


* Disclaimer
Parts of the material presented in these slides is

Copyright © Software Carpentry


* Preparation

1. Make a new folder on your Desktop called =pdap-03=
2. Download the test data from http://unihb.eu/gpSSQdTW, and unzip that file
   into the new folder (double-click or right-click to extract)

* Today's Lecture

- We will use /IPython/.  So please start the application called /IPython qtconsole/.
  - Start the application from the finder / start menu / unitiy
  - *OR* open a terminal (command window) and enter =ipython qtconsole=
- Type =%cd Desktop/pdap-03= to change into the folder with the test data
- Today's lecture material is again from /Software Carpentry/, at http://unihb.eu/gNkTMEF2

* Reading material: Lecture Notes

- Today's lecture is based on the Python course from Software Carpentry:
  https://swcarpentry.github.io/python-novice-inflammation/
- An excellent collection of tutorials is http://www.scipy-lectures.org/
- [[http://nbviewer.ipython.org/github/jrjohansson/scientific-python-lectures/tree/master]]

* Reading material: Books

- http://www.diveintopython3.net/
- http://docs.python-guide.org/en/latest/
- http://greenteapress.com/wp/think-python/

* Homework

- Will be distributed tomorrow via Stud.IP
- When inviting me to your project on GitLab, please make sure to give at least
  /Reporter/ permissions to me (as a /Guest/ I cannot see your files)

* Consultation hours
- 11 Nov (Friday) 10:10 - 11:30 
- 16 Nov (Wednesday) 16:30 - 17:50
- Registration: http://unihb.eu/ZiRGDa0g
