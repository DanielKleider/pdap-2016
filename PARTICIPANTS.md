Title: Participant list

Please CAPITALIZE your last name, like this: *Firstname LASTNAME*.

| Name            | GitLab username | Study program | OS    | Architecture | Python version |
|-----------------|-----------------|---------------|-------|--------------|----------------|
| Andreas HILBOLL | andreas-h       | *Lecturer*    | Linux | 64bit        |            3.5 |
| Daniel KLEIDER  | DanielKleider   | Bionik M.Sc.  | Win   | 64bit        |            3.5 |